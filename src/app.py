from flask import Flask, g, make_response, jsonify

app = Flask(__name__)


def get_db():
    return {
        "employees": [
            {"id": "farooqam", "name": "Farooq Mahmud", "salary": "150000"},
            {"id": "janedoe", "name": "Jane Doe", "salary": "88000"}
        ]
    }


@app.before_request
def initialize_db():
    g.db = get_db()


@app.route("/api/employees")
def get_employee():
    employees = g.db.get("employees")
    return make_response(jsonify(employees)), 200


@app.route("/api/employees/<id>")
def get_employee_by_id(id):
    employees = g.db.get("employees")
    employee = [e for e in employees if e["id"] == id]
    return make_response(jsonify(employee)), 200


if __name__ == "__main__":
    app.run(debug=True, port=8888)
