from mockito import *
import unittest
import src.app
from flask import json


class ApiUnitTests(unittest.TestCase):
    def setUp(self):
        self.app = src.app.app.test_client()
        self.db = mock()
        src.app.get_db = lambda: self.db

        self.expected_employees = [
            {"id": "emp1", "name": "Employee 1", "salary": "100000"},
            {"id": "emp2", "name": "Employee 2", "salary": "50000"}
        ]

    def test_get_employees_returns_all_employees(self):
        when(self.db).get("employees").thenReturn(self.expected_employees)
        response = self.app.get("api/employees")
        self.assertEqual(response.status_code, 200)

        employees = json.loads(response.data)

        self.assertEqual(len(employees), 2)

    def test_get_employee_by_id_returns_the_correct_employee(self):
        when(self.db).get("employees").thenReturn(self.expected_employees)
        response = self.app.get("api/employees/" + "emp2")
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.data)
        self.assertEqual(content[0]["id"], "emp2")


if __name__ == "__main__":
    unittest.main()
